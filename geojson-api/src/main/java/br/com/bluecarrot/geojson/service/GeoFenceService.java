package br.com.bluecarrot.geojson.service;

import org.geojson.GeometryCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.bluecarrot.geojson.domain.Polygon;
import br.com.bluecarrot.geojson.exception.ObjectNotFoundException;
import br.com.bluecarrot.geojson.repository.GeoFenceRepository;

/**
 * 
 * Classe da camada de serviço responsavel por intermediar o conteudo entre a
 * camadas de view (Controllers) e banco (Repository).
 * 
 * @author Jair Batista Junior
 *
 */
@Service
public class GeoFenceService {

	@Autowired
	private GeoFenceRepository geoFenceRepository;

	/**
	 * Cria uma nova area
	 * 
	 * @param polygon
	 * @return
	 */
	public Polygon create(Polygon polygon) {

		return geoFenceRepository.save(polygon);
	}

	/**
	 * Retorna coleção de objetos do tipo poligono
	 * 
	 * @return
	 * @throws ObjectNotFoundException
	 */
	public GeometryCollection readAll() throws ObjectNotFoundException {

		Iterable<Polygon> polygons = geoFenceRepository.findAll();

		if (polygons != null) {
			GeometryCollection geometryCollection = new GeometryCollection();
			for (Polygon polygon : polygons) {
				geometryCollection.add(polygon);
			}
			return geometryCollection;
		}

		throw new ObjectNotFoundException("No records found.");
	}

	/**
	 * Retorna um objeto do tipo Poligono, basedo em um id
	 * 
	 * @param id
	 * @return
	 * @throws ObjectNotFoundException
	 */
	public Polygon read(String id) throws ObjectNotFoundException {

		Polygon result = geoFenceRepository.findOne(id);
		if (result != null) {

			return result;
		}

		throw new ObjectNotFoundException("Record not found by ID:", id);
	}

	/**
	 * Faz a atualização de poligono já existente, porem caso ele não exista uma
	 * exceção é lançada para a camada superior
	 * 
	 * @param polygon
	 * @throws ObjectNotFoundException
	 */
	public Polygon update(String id, Polygon polygon) throws ObjectNotFoundException {

		if (geoFenceRepository.exists(id)) {
			polygon.setId(id);
			return geoFenceRepository.save(polygon);
		}

		throw new ObjectNotFoundException("Record not exists to do update:", id);
	}

	/**
	 * Remove uma area baseado no id passado como paramentro
	 * 
	 * @param id
	 * @return
	 * @throws ObjectNotFoundException
	 */
	public void delete(String id) throws ObjectNotFoundException {

		if (geoFenceRepository.exists(id)) {
			geoFenceRepository.delete(id);
		}

		throw new ObjectNotFoundException("Unable to delete record. Register not found.");
	}

}
