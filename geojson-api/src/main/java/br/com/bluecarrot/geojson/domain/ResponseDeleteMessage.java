package br.com.bluecarrot.geojson.domain;

/**
 * Classe que representa uma mensagem de informaçãoa quando um objeto é deletado
 * ou não.
 * 
 * @author Jair Batista Junior
 *
 */
public class ResponseDeleteMessage implements JsonObjectId<String> {

	private String id;
	private boolean deleted;

	public ResponseDeleteMessage(String id, boolean deleted) {
		this.id = id;
		this.deleted = deleted;
	}

	/**
	 * 
	 */
	@Override
	public String getId() {
		return id;
	}

	/**
	 * 
	 */
	@Override
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isDeleted() {
		return deleted;
	}

	/**
	 * 
	 * @param deleted
	 */
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

}
