package br.com.bluecarrot.geojson;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Classe principal que inicia o contexto do Spring
 * 
 * @author Jair Batista Junior
 *
 */
@SpringBootApplication
@EnableAutoConfiguration
public class AppContext {

	private static final Logger LOG = LoggerFactory.getLogger(AppContext.class);

	public static void main(String[] args) {

		LOG.info("Iniciando contexto da aplicação");
		SpringApplication.run(AppContext.class, args);
	}
}
