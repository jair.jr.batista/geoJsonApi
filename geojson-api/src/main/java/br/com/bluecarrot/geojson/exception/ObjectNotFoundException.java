package br.com.bluecarrot.geojson.exception;

/**
 * Exceção disparada quando um objeto não é encontrado.
 * 
 * @author Jair Batista Junior
 *
 */
public class ObjectNotFoundException extends Exception {

	private static final long serialVersionUID = 1L;

	/**
	 * Cria uma exceção de objeto não encontrado, contendo uma mensagem
	 * especifica
	 * 
	 * @param message
	 */
	public ObjectNotFoundException(String message) {
		this(message, "");
	}

	/**
	 * Cria uma exceção de objeto não encontrado, contendo uma mensagem
	 * especifica e informa o id do objeto buscado
	 * 
	 * @param message
	 */
	public ObjectNotFoundException(String message, String objectId) {
		super((message + " " + objectId).trim());
	}

}
