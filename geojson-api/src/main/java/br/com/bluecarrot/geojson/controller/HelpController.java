package br.com.bluecarrot.geojson.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Classe de suporte que serve apenas para teste, no qual consulta se o serviço
 * está funcionando
 * 
 * @author Jair Batista Junior
 *
 */
@RestController
public class HelpController {

	/**
	 * Retorna na pagina o conteudo "It`s works", quando o serviço está
	 * funcionando
	 * 
	 * @return
	 */
	@RequestMapping("/whatStatusApi")
	@ResponseBody
	private String whatStatusApi() {

		return "It`s works";
	}

}
