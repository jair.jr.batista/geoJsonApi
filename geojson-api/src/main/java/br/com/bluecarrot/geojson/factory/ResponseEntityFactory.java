package br.com.bluecarrot.geojson.factory;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * Constroi os objetos de resposta resposaveis pelo retono das entidades
 * solicitadas durante a requisição
 * 
 * 
 * @author Jair Batista Junior
 *
 * @param <T>
 */
public class ResponseEntityFactory<T> {

	/**
	 * Cria uma resposta de sucesso, contendo uam entida especifica
	 * 
	 * @param entity
	 * @return
	 */
	public ResponseEntity<T> createSuccessResponse(T entity) {
		return new ResponseEntity<T>(entity, HttpStatus.OK);
	}

	/**
	 * Cria uma resposta de erro, contendo uma entidade especifica
	 * 
	 * @param entity
	 * @param status
	 * @return
	 */
	public ResponseEntity<T> createErrorResponse(T entity, HttpStatus status) {
		return new ResponseEntity<T>(entity, status);

	}

}
