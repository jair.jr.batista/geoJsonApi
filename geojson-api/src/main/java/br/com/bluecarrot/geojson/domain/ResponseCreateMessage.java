package br.com.bluecarrot.geojson.domain;

/**
 * Classe que representa uma mensagem de confirmação da inserção de uma nova
 * area
 * 
 * @author Jair Batista Junior
 *
 */
public class ResponseCreateMessage implements JsonObjectId<String> {

	private String id;

	public ResponseCreateMessage(String id) {
		this.id = id;
	}

	/**
	 * Retorna um ID do tipo String
	 */
	@Override
	public String getId() {
		return this.id;
	}

	/**
	 * Seta um ID do tipo String
	 */
	@Override
	public void setId(String id) {
		this.id = id;
	}

}
