package br.com.bluecarrot.geojson.domain;

/**
 * Interface resposavel por agregar um ID as classe que à implementama
 * 
 * @author Jair Batista Junior
 *
 * @param <T>
 */
public interface JsonObjectId<T> {

	/**
	 * 
	 * @return Retorna um ID basedo no tipo generico "T"
	 */
	T getId();

	/**
	 * 
	 * @param t Seta um paramentro basedo no tipo generico "T"
	 */
	void setId(T t);

}
