package br.com.bluecarrot.geojson.controller;

import org.geojson.GeometryCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.bluecarrot.geojson.domain.Polygon;
import br.com.bluecarrot.geojson.domain.ResponseCreateMessage;
import br.com.bluecarrot.geojson.domain.ResponseDeleteMessage;
import br.com.bluecarrot.geojson.domain.ResponseErrorMessage;
import br.com.bluecarrot.geojson.exception.ObjectNotFoundException;
import br.com.bluecarrot.geojson.factory.ResponseEntityFactory;
import br.com.bluecarrot.geojson.service.GeoFenceService;

/**
 * Classe responsavel pela controle da API GeoJson, contendo os metodos
 * necessário para criar, lista, alterar e deletar uma area do tipo Poligono.
 * Mais detalhes sobre a especificação, consulte a documentação no link
 * <a href="http://geojson.org/geojson-spec.html">GeoJson</a>.
 * 
 * @author Jair Batista Junior
 */
@RestController
@RequestMapping(path = "/fence", produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {
		MediaType.APPLICATION_JSON_VALUE })

public class GeoFenceController {

	private static final Logger LOG = LoggerFactory.getLogger(GeoFenceController.class);

	@Autowired
	private GeoFenceService geoFenceService;

	private ResponseEntityFactory<Object> responseFactory = new ResponseEntityFactory<>();

	/**
	 * Cria uma nova area baseada em um poligono seguindo a especificação do
	 * GeoJson, veja <a href="http://geojson.org/geojson-spec.html#id4">GeoJson
	 * Polygon</a>.
	 * 
	 * @param polygon
	 * @return O retorno é um objeto do tipo Json, que identifica a area criada
	 *         pela API, Ex.: <br>
	 *         {<br>
	 *         "id":"588fda23a666f60f6a5557e3"<br>
	 *         }
	 */
	@RequestMapping(path = "/create", method = RequestMethod.POST)
	private ResponseEntity<?> create(@RequestBody Polygon polygon) {
		LOG.info("POST - patch=*/create - [ method - addFence()]");

		if (polygon.isValidObject()) {
			Polygon result = geoFenceService.create(polygon);
			ResponseCreateMessage response = new ResponseCreateMessage(result.getId());
			return responseFactory.createSuccessResponse(response);
		}

		return responseError("GeoJson object inválid.", HttpStatus.BAD_REQUEST);
	}

	/**
	 * Retorna toda a lista de areas no formato GeometryCollection, conforme a
	 * especificação
	 * <a href="http://geojson.org/geojson-spec.html#geometrycollection">GeoJson
	 * GeometryCollection.</a>
	 * 
	 * @return
	 */
	@RequestMapping(path = "/records", method = RequestMethod.GET)
	private ResponseEntity<?> listAllFences() {
		LOG.info("GET - patch=*/records - [ method - listAllFences()]");

		try {
			GeometryCollection geometryCollection = geoFenceService.readAll();
			return responseFactory.createSuccessResponse(geometryCollection);

		} catch (ObjectNotFoundException e) {
			return responseError(e.getMessage(), HttpStatus.NOT_FOUND);
		}
	}

	/**
	 * Retorna um unico objeto do tipo Poligono, como especificado em
	 * <a href="http://geojson.org/geojson-spec.html#id4">GeoJson Polygon</a>,
	 * basedo no idenficador passado na URL como parametro.
	 * 
	 * @param id
	 *            ../fence/read/588fda23a666f60f6a5557e3
	 * 
	 * @return {@link Polygon}
	 */
	@RequestMapping(path = "/read/{id}", method = RequestMethod.GET)
	private ResponseEntity<?> get(@PathVariable("id") String id) {
		LOG.info("GET - patch=*/read/{id} - [ method - get()]");

		try {
			Polygon polygon = geoFenceService.read(id);
			return responseFactory.createSuccessResponse(polygon);

		} catch (ObjectNotFoundException e) {
			return responseError(e.getMessage(), HttpStatus.NOT_FOUND);
		}

	}

	/**
	 * Metodo responsavel pela alteração de uma area, baseado no id passado como
	 * paramentro, quando o id não é encontrado uma exceção é gerada informando
	 * que nenhum objeto foi encontrado para o Id em questão, um objeto do tipo
	 * Poligono tambem deve ser passado no corpo da requisição para que o objeto
	 * seja atualizado conforme os novos paramentros. *
	 * 
	 * @param id
	 *            ../fence/read/588fda23a666f60f6a5557e3
	 * @param polygon
	 *            {@link Polygon}
	 * 
	 * @return {@link Polygon}
	 */
	@RequestMapping(path = "/update/{id}", method = RequestMethod.PUT)
	private ResponseEntity<?> update(@PathVariable("id") String id, @RequestBody Polygon polygon) {
		LOG.info("PUT - patch=*/update/{id} - [ method - update()]");

		try {
			Polygon result = geoFenceService.update(id, polygon);
			return responseFactory.createSuccessResponse(result);

		} catch (ObjectNotFoundException e) {
			return responseError(e.getMessage(), HttpStatus.NOT_FOUND);
		}

	}

	/**
	 * Metodo responsavel pela remoção de uma area do tipo Poligono, basedo no
	 * id passado como paramentro na URL.
	 * 
	 * @param id
	 *            ../fence/read/588fda23a666f60f6a5557e3
	 * @return Objeto em json, informando o status da operação. Ex.: <br>
	 *         {<br>
	 *         "id": "588fda23a666f60f6a5557e3",<br>
	 *         "deleted": true<br>
	 *         }
	 */
	@RequestMapping(path = "/delete/{id}", method = RequestMethod.DELETE)
	private ResponseEntity<?> delete(@PathVariable("id") String id) {
		LOG.info("DELETE - patch=*/delete/{id} - [ method - delete()]");

		try {
			geoFenceService.delete(id);
			return responseFactory.createSuccessResponse(new ResponseDeleteMessage(id, true));

		} catch (ObjectNotFoundException e) {
			return responseError(e.getMessage(), HttpStatus.NOT_FOUND);
		}

	}

	/**
	 * Retorna uma resposta de erro padrão baseada na classe
	 * {@link ResponseErrorMessage}, contendo uma mensagem que especifica o
	 * problema
	 * 
	 * @param message
	 *            Mensagem de erro que foi gerada
	 * @param status
	 *            {@link HttpStatus}, informando o codigo do problema
	 * @return
	 */
	private ResponseEntity<Object> responseError(String message, HttpStatus status) {
		LOG.error("[ method - responseError() ] - {}", message);
		return responseFactory.createErrorResponse(new ResponseErrorMessage(message), status);
	}

}
