package br.com.bluecarrot.geojson.domain;

import java.util.List;

import org.geojson.LngLatAlt;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Objeto de dominio responsavel pelo parse de dados entre o mundo externo e a
 * aplicação, esta classe representa em sua totalidade o conteudo do objeto
 * "GeoJson Polygon", conforme a especificação
 * <a href="http://geojson.org/geojson-spec.html#id4">GeoJson Polygon</a>
 * 
 * @author Jair Batista Junior
 */
@Document
public class Polygon extends org.geojson.Polygon implements JsonObjectId<String> {

	private static final long serialVersionUID = 1L;
	private String id;

	/**
	 * Retorna o id deste objeto depois de persistido no banco, este parametro é
	 * ignorado pela Api quando trnaformado em Json
	 */
	@Id
	@JsonIgnore
	@Override
	public String getId() {

		return this.id;
	}

	/**
	 * Insere um Id para este objeto, em geral este parametro é preencido
	 * automaticamente pelo banco de dados, contudo ele pode ser usado durante o
	 * processo de alteração de um cadastro
	 */
	@Override
	public void setId(String id) {

		this.id = id;
	}

	/**
	 * Verifica se este é um poligono valido, isso ocorre quando o objeto possui
	 * mais de 3 pontos
	 * 
	 * @return
	 */
	@JsonIgnore
	public boolean isValidObject() {

		boolean isValid = true;

		// Faz a varredura de todas as coordenadas
		for (List<LngLatAlt> internalCoordinates : coordinates) {

			if (internalCoordinates.size() < 3) {
				isValid = false;
			}
		}

		return isValid;
	}

}
