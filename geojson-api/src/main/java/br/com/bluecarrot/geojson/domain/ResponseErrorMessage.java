package br.com.bluecarrot.geojson.domain;

/**
 * Classe que representa uma mensagem de retorno de erro
 * 
 * @author Jair Batista Junior
 *
 */
public class ResponseErrorMessage {

	private String message;

	/**
	 * 
	 * @param message
	 */
	public ResponseErrorMessage(String message) {
		this.message = message;
	}

	/**
	 * 
	 * @return
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * 
	 * @param message
	 */
	public void setMessage(String message) {
		this.message = message;
	}

}
