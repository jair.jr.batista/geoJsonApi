package br.com.bluecarrot.geojson.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.bluecarrot.geojson.domain.Polygon;

/**
 * Interface responsavel pelas operações de CRUD.
 * 
 * @author Jair Batista Junior
 *
 */
@Repository
public interface GeoFenceRepository extends CrudRepository<Polygon, String> {

}
