GeoJson - API
Usando Java, Spring e MongoDB

Para testar faça a instalaçao do MongoDB na sua maquina, ou caso faça uso do Docker, use o comando abaixo.

Docker: docker run -d -v /Users/jair/diretorio-externo:/data/db --name mymongo -p 27017:27017 -d mongo:latest

API

Create:
Ex.: Post -> http://localhost:8080/fence/create (Headers: Content-Type: application/json)
    Body:
    { "type": "Polygon",
        "coordinates": [
            [ 
    	        [100.0, 0.0],
    	        [101.0, 0.0],
    	        [101.0, 1.0],
    	        [100.0, 1.0],
    	        [100.0, 0.0]
            ]
        ]
    }

Read:
Ex.: Get -> http://localhost:8080/fence/records (Headers: Content-Type: application/json)
    Body:
    {
        "type": "GeometryCollection",
        "geometries": [
            {
                "type": "Polygon",
                "coordinates": [
                    [
                        [100,0],
                        [101,0],
                        [101,1],
                        [100,1],
                        [100,0]
                    ]
                ]
            }
        ]
    }

Ex.: Get -> http://localhost:8080/fence/read/588fda23a666f60f6a5557e3 (Headers: Content-Type: application/json)
    Body:
    {
        "type": "Polygon",
        "coordinates": [
            [
                [100,0],
                [101,0],
                [101,1],
                [100,1],
                [100,0]
            ]
        ]
    }
    
Update:
Ex.: Post -> http://localhost:8080/fence/update/588fda23a666f60f6a5557e3 (Headers: Content-Type: application/json)
    Body:
    {
        "type": "Polygon",
        "coordinates": [
            [ 
        		[500.0, 100.0],
        		[501.0, 100.0],
        		[501.0, 101.0],
        		[500.0, 101.0],
        		[500.0, 100.0]
        	]
        ]
    }

Delete:
Ex.: Delete -> http://localhost:8080/fence/delete/588fda23a666f60f6a5557e3 (Headers: Content-Type: application/json)
    Body:
    {
        "id": "588fda23a666f60f6a5557e3",
        "deleted": true
    }